/**
 * Identity function
 *
 * @param  {mixed}  source  Source input
 *
 * @return {mixed}
 */
const i = source => source

export { i }
