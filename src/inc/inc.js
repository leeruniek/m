/**
 * Add one
 *
 * @param  {number}  source  Source input
 *
 * @return {number}
 *
 * @tag Number
 * @signature (source: number): number
 *
 * @example
 * inc(2)
 * // => 3
 */
const inc = source => source + 1

export { inc }
