/**
 * Substract one
 *
 * @param  {number}  source  Source input
 *
 * @return {number}
 *
 * @tag Number
 * @signature (source: number): number
 *
 * @example
 * dec(2)
 * // => 1
 */
const dec = source => source - 1

export { dec }
